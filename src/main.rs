/**
Stats about DPL usage on Wikimedia wikis
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use serde::{Deserialize, Serialize};
use tokio::fs;

const CATEGORY_MSG: &str = "intersection-category";
const USER_AGENT: &str = toolforge::user_agent!("dpl-tracker");

#[derive(Serialize, Debug)]
struct WikiEntry {
    name: String,
    enabled: bool,
    categoryinfo: Category,
}

#[derive(Deserialize)]
struct Config {
    wikis: Vec<String>,
}

#[derive(Deserialize)]
struct Siteinfo {
    query: SiteinfoQuery,
}

#[derive(Deserialize)]
struct SiteinfoQuery {
    extensions: Vec<Extension>,
}

#[derive(Deserialize)]
struct Extension {
    name: String,
}

#[derive(Deserialize)]
struct AllMessages {
    query: AllMessagesQuery,
}

#[derive(Deserialize)]
struct AllMessagesQuery {
    allmessages: Vec<Message>,
}

#[derive(Deserialize)]
struct Message {
    content: String,
}

#[derive(Deserialize)]
struct CategoryInfo {
    query: CategoryInfoQuery,
}

#[derive(Deserialize)]
struct CategoryInfoQuery {
    pages: Vec<CategoryInfoPage>,
}

#[derive(Deserialize)]
struct CategoryInfoPage {
    categoryinfo: Option<Category>,
}

#[derive(Default, Deserialize, Serialize, Debug, Clone, Copy)]
struct Category {
    size: usize,
    pages: usize,
    files: usize,
    subcats: usize,
}

async fn handle_wiki(wiki: &str) -> Result<WikiEntry> {
    let client =
        mwapi::Client::bot_builder(&format!("https://{}/w/api.php", wiki))
            .set_user_agent(USER_AGENT)
            .build()
            .await?;
    let siteinfo: Siteinfo = serde_json::from_value(
        client
            .get(&[
                ("action", "query"),
                ("meta", "siteinfo"),
                ("siprop", "extensions"),
            ])
            .await?,
    )?;
    if !is_dpl_enabled(&siteinfo) {
        return Ok(WikiEntry {
            name: wiki.to_string(),
            enabled: false,
            categoryinfo: Category::default(),
        });
    }

    let allmessages: AllMessages = serde_json::from_value(
        client
            .get(&[
                ("action", "query"),
                ("meta", "allmessages"),
                ("ammessages", CATEGORY_MSG),
                ("amenableparser", "1"),
            ])
            .await?,
    )?;
    let cat_name = &allmessages.query.allmessages[0].content;
    let catinfo: CategoryInfo = serde_json::from_value(
        client
            .get(&[
                ("action", "query"),
                ("titles", &format!("Category:{}", cat_name)),
                ("prop", "categoryinfo"),
            ])
            .await?,
    )?;
    let category = catinfo.query.pages[0].categoryinfo.unwrap_or_default();
    Ok(WikiEntry {
        name: wiki.to_string(),
        enabled: true,
        categoryinfo: category,
    })
}

fn is_dpl_enabled(siteinfo: &Siteinfo) -> bool {
    for extension in &siteinfo.query.extensions {
        if extension.name == "DynamicPageList" {
            return true;
        }
    }

    false
}

#[tokio::main]
async fn main() -> Result<()> {
    let config: Config =
        toml::from_str(&fs::read_to_string("config.toml").await?)?;
    let mut entries = vec![];
    for wiki in config.wikis {
        let entry = handle_wiki(&wiki).await?;
        dbg!(&entry);
        entries.push(entry);
    }

    let json = serde_json::to_string(&entries)?;
    fs::write("stats.json", json.as_bytes()).await?;
    println!("Wrote to stats.json");

    Ok(())
}
